<?php

namespace Deployer;

require 'recipe/laravel.php';
require 'recipe/npm.php';

// Set SSH Multiplexing
set('ssh_multiplexing', true);

// Set default branch
set('branch', 'dev');

// Set git_tty
set('git_tty', false);

// Set php binary file path
set('bin/php', '/usr/bin/php7.2');

// Project name
set('application', 'sesa-living');

// Project repository
set('repository', 'git@***************.git');

// Shared files/dirs between deploys
add('shared_files', ['.env']);
add('shared_dirs', ['node_modules']);

// Writable dirs by web server
add('writable_dirs', []);

set('keep_releases', 5);

// Hosts
host('staging')
    ->hostname('167.172.76.143') // sesa suitdev.com
    ->forwardAgent(false)
    ->user('suitmedia')
    ->port(22)
    ->roles('app')
    ->stage('staging')
    ->set('deploy_path', '~/www/{{application}}');

host('production')
     ->hostname('104.248.96.189') // suitdev.com
     ->forwardAgent(false)
     ->user('suitmedia')
     ->port(22)
     ->roles('app')
     ->stage('production')
     ->set('deploy_path', '~/www/{{application}}');

// Tasks
// Override Tasks
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'npm:install',
    'npm:build',
    'artisan:laravolt'
]);

task('deploy:composer-command', function () {
        set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader');
})->onStage('staging');

task('artisan:clear-compiled', function () {
        run('{{bin/php}} {{release_path}}/artisan clear-compiled');
});

task('restart:web', function () {
    run('sudo service php7.3-fpm restart');
    run('sudo service nginx restart');
    // run('sudo service varnish restart');
})->onRoles('app', 'webserver');

task('restart:workers', function () {
        // specify commands to restart workers
})->onRoles('app', 'worker');

task('restart:services', ['restart:web', 'restart:workers']);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Update the task sequence
task('artisan:migrate', function () {
    run('{{bin/php}} {{release_path}}/artisan migrate --force');
    run('{{bin/php}} {{release_path}}/artisan migrate --force --path="database/migrations/migration_livings"');
})->once();

task('artisan:laravolt', function () {
    run('{{bin/php}} {{release_path}}/artisan laravolt:link-assets');
})->once();

// Build assets webpack
task('npm:build', function () {
    run("cd {{release_path}} && {{bin/npm}} run dev");
});

before('deploy:vendors', 'deploy:composer-command');
before('artisan:config:cache', 'artisan:clear-compiled');
// before('artisan:config:cache', 'artisan:route:cache');
// before('artisan:optimize', 'artisan:clear-compiled');
before('deploy:symlink', 'artisan:migrate');
before('success', 'restart:services');
